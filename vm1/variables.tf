variable "subnet_name" {
    default = "default"
}
variable "virtual_network_name" {
    default = "terraform"
}
variable "virtual_network_rg" {
    default = "terraform"
}
variable "resource_group_name" {
  
}
variable "location" {
    default = "westeurope"
}
variable "nsg_name" {
    default = "IsNotRequired"
}
variable "virtual_machine_name" {
  default = "vm1"
}
variable "ip_allocation" {
    default = "dynamic"  
}

variable "replication_type" {
  default = "LRS"
}
variable "replication_tier" {
  default = "Standard"
}
variable "vm_size" {
    default = "Standard_D2s_v3"
}
variable "image_publisher" {
    default = "Canonical"  
}
variable "image_offer" {
    default = "UbuntuServer"  
}
variable "image_sku" {
    default = "16.04.0-LTS"
}
variable "image_version" {
    default = "latest"
}
variable "disk_size" {
    default = "80"
}
variable "adminuser" {
    default = "david.tinoco"
}
variable "adminpass" {
    default = "P4$$w.rd"
}

variable "vm_tags" {
    type = "map"
    default = {
        schedulePlan = "P20"
    }
}

variable "public_ip_name" {
    default = "thepublicip"  
}

