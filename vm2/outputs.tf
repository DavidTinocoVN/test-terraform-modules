output "vm_id" {
  value = "${azurerm_virtual_machine.myvm.id}"
}

output "rsg_name" {
  value = "${azurerm_resource_group.myrsg.name}"
}

output "public_ip" {
  value = "${azurerm_public_ip.mypip.ip_address}"
}
