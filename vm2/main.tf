data "azurerm_subnet" "mysubnet"{
    name                    = "${var.subnet_name}"
    virtual_network_name    = "${var.virtual_network_name}"
    resource_group_name     = "${var.virtual_network_rg}"
}

resource "azurerm_resource_group" "myrsg" {
    name        = "${var.resource_group_name}"  
    location    = "${var.location}"
}

resource "azurerm_network_security_group" "mynsg" {
    name       			= "${var.virtual_machine_name}-NSG"
    location    		= "${var.location}"
    resource_group_name = "${azurerm_resource_group.myrsg.name}"

	security_rule {
		name = "testssh"
		priority = 100
		direction = "Inbound"
		access = "Allow"
		protocol = "Tcp"
		destination_port_range = "22"
		source_port_range = "*"
		source_address_prefix = "*"
		destination_address_prefix = "*"

	}
	security_rule {
		name = "testweb"
		priority = 101
		direction = "Inbound"
		access = "Allow"
		protocol = "Tcp"
		destination_port_range = "80"
		source_port_range = "*"
		source_address_prefix = "*"
		destination_address_prefix = "*"
	}
}
resource "azurerm_public_ip" "mypip" {
	  name					= "${var.public_ip_name}"
	  location 				= "${var.location}"
	  resource_group_name 	= "${azurerm_resource_group.myrsg.name}"
	  allocation_method 	= "Dynamic"
	  
}
resource "azurerm_network_interface" "mynic" {
	name						= "${var.virtual_machine_name}-NIC01"
	location					= "${var.location}"
	resource_group_name			= "${azurerm_resource_group.myrsg.name}"
	network_security_group_id	= "${azurerm_network_security_group.mynsg.id}"

	ip_configuration {
		name							= "ipconfig1"
		subnet_id						= "${data.azurerm_subnet.mysubnet.id}"
		private_ip_address_allocation	= "${var.ip_allocation}"
		public_ip_address_id 			= "${azurerm_public_ip.mypip.id}"
	}
}

resource "azurerm_storage_account" "SuiteStorageAccount" {
	name						= "${lower(var.virtual_machine_name)}diag"
	resource_group_name			= "${azurerm_resource_group.myrsg.name}"
	location					= "${var.location}"
	account_replication_type	= "${var.replication_type}"
	account_tier				= "${var.replication_tier}"

}
resource "azurerm_virtual_machine" "myvm" {
	name					= "${var.virtual_machine_name}"
	location				= "${var.location}"
	resource_group_name		= "${azurerm_resource_group.myrsg.name}"
	network_interface_ids	= ["${azurerm_network_interface.mynic.id}"]
	vm_size					= "${var.vm_size}"

	storage_os_disk {
		name				= "${var.virtual_machine_name}-OSDISK-01"
        disk_size_gb        = "${var.disk_size}"
		caching				= "ReadWrite"
		create_option		= "FromImage"
		managed_disk_type	= "${var.replication_tier}_${var.replication_type}"
	}

	storage_image_reference {
		publisher	= "${var.image_publisher}"
		offer		= "${var.image_offer}"
		sku			= "${var.image_sku}"
		version		= "${var.image_version}"
	}

	os_profile {
		computer_name	= "${var.virtual_machine_name}"
		admin_username	= "${var.adminuser}"
		admin_password	= "${var.adminpass}"
	}

	os_profile_linux_config {
		disable_password_authentication = true
		ssh_keys {
			path = "/home/${var.adminuser}/.ssh/authorized_keys"
			key_data = "${file("~/.ssh/id_rsa.pub")}"
		}
	}

	boot_diagnostics {
		enabled		= "true"
		storage_uri	= "${azurerm_storage_account.SuiteStorageAccount.primary_blob_endpoint}"
	}

	tags = "${var.vm_tags}"
}